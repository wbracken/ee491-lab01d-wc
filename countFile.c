#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <stdbool.h>
#include "getopt.h"
#include <unistd.h>
#include <fcntl.h>



char** countFile(char* fileName, bool printBytes, bool printLines, bool printWords) {

      int linect, wordct, charct;
      int pFile;


      if (fileName == NULL) {
            pFile = STDIN_FILENO;
      } else {
         pFile = fopen(fileName, O_RDONLY, 0);
      }

   
      if (pFile == -1){
         printf("[%s]: Can't open [%s]\n", programName, fileName);
         return EXIT_FAILURE; 
      }
   
   //scan FILE for chars, increment for each char found, stop at EOF
   while ((c = fgetc(pFile)) != EOF) {
      charct++;

   //check for new lines, increment for each new line
      if (c == '\n') {
         linect++;
         }
   
   /*check if char is whitespace. 
    * if char is not whitespace, indicate word found. 
    * when no longer in word increment word count 
    * clear word found flag.*/

      if (isspace(c)){
         if (in_word){
            in_word = false;
            wordct++;
         }
      } else {
         in_word = true;
         }
   }

      if (printBytes){
        printf("%d\t", charct);
      } 
      if (printLines){
        printf("%d\t", linect);
      }
      if (printWords){
        printf("%d\t", wordct);
      } 
      if(fileName != NULL) {
        printf("%s\n", fileName);
    } else {
        printf("%s\n", "from stdin");
    }


   fclose (pFile);

   return EXIT_SUCCESS;
}


static void usage() {
    (void)fprintf(stderr, "usage: wc [-clwv] [file ...]\n");
    exit ( EXIT_FAILURE );
}