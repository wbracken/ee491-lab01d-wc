/////////////////////////////////////////////////////////////////////////////////
//           University of Hawaii, College of Engineering
/// @brief   Lab01d - wc - EE 491 - Spr 2022
///
/// @file    wc.c
/// @version 1.1 - wc 2 lab assignment
///
/// wc - print newline, word, and byte counts for each file
///
/// @author  Will Bracken <wbracken@hawaii.edu>
/// @date    26_Jan_2022
///
/// @see     https://linuxcommand.org/lc3_man_pages/wc1.html
///
///////////////////////////////////////////////////////////////////////////////
#include <stdlib.h>
#include <stdio.h>
#include <ctype.h>
#include <stdbool.h>
#include "getopt.h"
#include <unistd.h>
#include <fcntl.h>
//#include "countFile.h"


#define versionNumber "version 1.1"
#define authorName "Will Bracken"
#define programName "wc.c"
bool printBytes, printLines, printWords, in_word;


static struct option const longopts[] =
{
  {"bytes", no_argument, NULL, 'c'},
  {"lines", no_argument, NULL, 'l'},
  {"words", no_argument, NULL, 'w'},
  {"version", no_argument, NULL, 'v'},
  {NULL, 0, NULL, 0}
};

char** countFile(char* fileName, bool printBytes, bool printLines, bool printWords);
void usage() ;


int main(int argc, char* argv[]){  

   int i, modeSelect; 
   int optInd = optind;
   printBytes = printLines = printWords = in_word= false;

while ((modeSelect = getopt_long(argc, argv, "clwv", longopts, NULL)) != -1) {
   switch (modeSelect){

   case 'c':
      printBytes = true;
      break;

  case 'l':
      printLines = true;
      break;
      
  case 'w':
      printWords = true;
      break;

  case 'v':
      printf("%s %s\nwritten by %s\n", programName, versionNumber, authorName);
      exit(EXIT_SUCCESS);
      break;

  default: usage();
   }
 if (! (printLines || printWords || printBytes))
    printLines = printWords = printBytes = true;


argc -= optInd;
argv += optInd;

}


   //check if a FILE is on command line

   for (i =0; i<argc; i++) {  
      if (argc > optInd) {
          if (!*argv) {
            printf("usage : [%s]\n", argv[i]);
         } else {
            countFile(argv[i], printBytes, printLines, printWords);
         }
      } else {
      countFile(argv[i], printBytes, printLines, printWords); 
      }
   
      return EXIT_SUCCESS;
   }
}

char** countFile(char* fileName, bool printBytes, bool printLines, bool printWords) {

      int c, linect=0, wordct=0, charct=0;
      FILE* pFile;


      if (fileName == NULL) {
            pFile = STDIN_FILENO;
      } else {
         pFile = fopen(fileName, O_RDONLY);
      }

   
      if (pFile == NULL){
         printf("[%s]: Can't open [%s]\n", programName, fileName);
         exit(EXIT_FAILURE); 
      }
   
   //scan FILE for chars, increment for each char found, stop at EOF
   while ((c = fgetc(pFile)) != EOF) {
      charct++;

   //check for new lines, increment for each new line
      if (c == '\n') {
         linect++;
         }
   
   /*check if char is whitespace. 
    * if char is not whitespace, indicate word found. 
    * when no longer in word increment word count 
    * clear word found flag.*/

      if (isspace(c)){
         if (in_word){
            in_word = false;
            wordct++;
         }
      } else {
         in_word = true;
         }
   }

      if (printBytes){
        printf("%d\t", charct);
      } 
      if (printLines){
        printf("%d\t", linect);
      }
      if (printWords){
        printf("%d\t", wordct);
      } 
      if(fileName != NULL) {
        printf("%s\n", fileName);
    } else {
        printf("%s\n", "from stdin");
    }


   fclose (pFile);

   return EXIT_SUCCESS;
}


void usage() {
    fprintf(stderr, "usage: wc [-clwv] [file ...]\n");
    exit ( EXIT_FAILURE );
}

