###############################################################################
###          University of Hawaii, College of Engineering
### @brief   Lab 01d - wc - EE 205 - Spr 2022
###
### @file    Makefile
### @version 1.1 - word count 2 lab
###
### Build and test Word Counting program
###
### @author  Will Bracken <wbracken@hawaii.edu>
### @date    26_Jan_2022
###
### @see     https://www.gnu.org/software/make/manual/make.html
###############################################################################

CC     = gcc
CFLAGS = -g -Wall

TARGET = wc

all: $(TARGET)

wc: wc.c
	$(CC) $(CFLAGS) -o $(TARGET) wc.c

test: wc
	./wc testFiles/README
	wc   testFiles/README

	./wc testFiles/test0
	wc   testFiles/test0

	./wc testFiles/test1
	wc   testFiles/test1

	./wc testFiles/test2
	wc   testFiles/test2

	./wc testFiles/test3
	wc   testFiles/test3

	./wc testFiles/test4
	wc   testFiles/test4

	./wc testFiles/test5
	wc   testFiles/test5

	./wc testFiles/test6
	wc   testFiles/test6

	./wc testFiles/test7
	wc   testFiles/test7

	./wc testFiles/test8
	wc   testFiles/test8

	./wc testFiles/test9
	wc   testFiles/test9

	./wc testFiles/testa
	wc   testFiles/testa


clean:
	rm -f $(TARGET)

